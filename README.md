# littlefs-test

I made this simple little testing program for [littlefs](https://github.com/ARMmbed/littlefs) so I can learn how the filesystem behaves and works
on low-level.

This filesystem runs on a dummy "blockdevice" generated in a normal binary file, so we can
inspect the contents of the file very easily with an hex editor or such.

### Usage

Create 1M filesystem file named "filesystem"

`dd if=/dev/zero of=filesystem bs=512 count=2048`

Then just run `littlefs-test`

**TIP!** Use `xxd filesystem` to inspect the filesystem contents in hex.

### Todo

Lot's of things to be done:

* Add more commands
* Maybe better command interpreter
* Automatic events from error codes, eg. `ls filename` could show the file instead of error
* etc...

### Example output
```
% ls
drw-	   0	.
drw-	   0	..
Total filesystem size is 2 bytes
% touch empty_file
% ls
drw-	   0	.
drw-	   0	..
-rw-	   0	empty_file
Total filesystem size is 2 bytes
% write data_file some data you want to have in the file
38 bytes writen
% ls
drw-	   0	.
drw-	   0	..
-rw-	  38	data_file
-rw-	   0	empty_file
Total filesystem size is 3 bytes
% cat empty_file
0 bytes read
% cat data_file
some data you want to have in the file
38 bytes read
% mkdir directory 
% ls
drw-	   0	.
drw-	   0	..
-rw-	  38	data_file
drw-	   0	directory
-rw-	   0	empty_file
Total filesystem size is 5 bytes
% write directory/new_file  
0 bytes writen
% ls directory
drw-	   0	.
drw-	   0	..
-rw-	   0	new_file
Total filesystem size is 5 bytes
```

### License

I don't have an license for littlefs-test. Only a disclaimer that I won't be responsible if
for example your computer sets on fire while using the program.

**However**, littlefs does have an license, so I must show it here.


>Copyright (c) 2017, Arm Limited. All rights reserved.
>
>Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
>
>Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
>Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
>Neither the name of ARM nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
>THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
