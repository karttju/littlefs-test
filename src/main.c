//
// Created by jussi on 25/10/2019.
//
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "lfs.h"

lfs_t lfs;
lfs_file_t file;
FILE *fp;

#define FILENAME "filesystem"

/**
 * Read a block
 * @param c lfs_config
 * @param block block address
 * @param off block offset
 * @param buffer read buffer
 * @param size size of the read
 * @return
 */
int bd_read(const struct lfs_config *c, lfs_block_t block,
            lfs_off_t off, void *buffer, lfs_size_t size) {
    //printf("reading block %u, offset %u\n", block, off);
    fseek(fp, block * c->block_size + off, SEEK_SET);
    fread(buffer, c->read_size, size / c->read_size, fp);
    return 0;
}

/**
 * Write a block
 * @param c lfs_config
 * @param block block address
 * @param off block offset
 * @param buffer write buffer
 * @param size size of the write
 * @return
 */
int bd_prog(const struct lfs_config *c, lfs_block_t block,
            lfs_off_t off, const void *buffer, lfs_size_t size) {
    //printf("prog block %u, offset %u\n", block, off);
    fseek(fp, (block * c->block_size + off), SEEK_SET);
    fwrite(buffer, c->prog_size, size / c->prog_size, fp);
    return 0;
}

/**
 * Erase a block.
 * @param c lfs_config
 * @param block block address
 * @return possible error code
 */
int bd_erase(const struct lfs_config *c, lfs_block_t block) {
    char array[512];
    memset(array, '\x00', sizeof(array));
    fseek(fp, block * c->block_size, SEEK_SET);
    fwrite(array, sizeof(array), 1, fp);
    return 0;
}

/**
 * Sync filesystem
 * @param c lfs_config
 * @return possible error code
 */
int bd_sync(const struct lfs_config *c) {
    fclose(fp);
    fp = fopen(FILENAME, "r+");
    return 0;
}

/**
 * Filesystem parameters
 */
const struct lfs_config cfg = {
        // block device operations
        .read  = bd_read,
        .prog  = bd_prog,
        .erase = bd_erase,
        .sync  = bd_sync,

        // block device configuration
        .read_size = 16,
        .prog_size = 16,
        .block_size = 512,
        .block_count = 2048,
        .cache_size = 16,
        .lookahead_size = 16,
        .block_cycles = 500,
};

/**
 * Mount a filesystem. The filesystem will be formatted if mount is not possible.
 * Use "safemount" to avoid the formatting.
 * @return possible error code
 */
int mount() {
    // mount the filesystem
    int err = lfs_mount(&lfs, &cfg);

    // reformat if we can't mount the filesystem
    // this should only happen on the first boot
    if (err) {
        printf("Filesystem not formatted, unable to mount. Formatting...\n");
        char array[512];
        memset(array, '\x00', sizeof(array));
        fseek(fp, 0, SEEK_SET);
        fwrite(array, sizeof(array), 2048, fp);
        lfs_format(&lfs, &cfg);
        lfs_mount(&lfs, &cfg);
    }
    return err;
}

/**
 * Unmount filesystem
 * @return possible error code
 */
int umount() {
    // release any resources we were using
    return lfs_unmount(&lfs);
}

/**
 * Open a file
 * @param path file path
 * @param flags open flags
 * @return possible error code
 */
int fileopen(char *path, enum lfs_open_flags flags) {
    // open a file
    int err = lfs_file_open(&lfs, &file, path, flags);
    return err;
}

/**
 * Close a file. Write operation is saved when file is closed
 * @return possible error code
 */
int fileclose() {
    // remember the storage is not updated until the file is closed successfully
    return lfs_file_close(&lfs, &file);
}

/**
 * Read data from a file
 * @param buffer return data to this buffer
 * @param size read size
 * @return possible error code or bytes read
 */
int fileread(char *buffer, lfs_size_t size) {
    // we don't use err here, because lfs_file_read can return positive number
    // indicating how many bytes were read
    int ret;
    // file is open, we can read it
    ret = lfs_file_read(&lfs, &file, buffer, size);
    return ret;
}

/**
 * Write data to a file
 * @param buffer data to write
 * @param size size of the data to write
 * @return possible error code or bytes written
 */
int filewrite(void *buffer, lfs_size_t size) {
    return lfs_file_write(&lfs, &file, buffer, size);
}

/**
 * Set file pointer to beginning of the file
 * @return possible error code
 */
int filerewind() {
    return lfs_file_rewind(&lfs, &file);
}

/**
 * List contents of a directory
 * @param path directory path
 * @return possible error code
 */
int lsdir(const char *path) {
    lfs_dir_t dir;
    int err = lfs_dir_open(&lfs, &dir, path);
    if (err) {
        return err;
    }

    struct lfs_info info;
    while (true) {
        int res = lfs_dir_read(&lfs, &dir, &info);
        if (res < 0) {
            return res;
        }

        if (res == 0) {
            break;
        }

        switch (info.type) {
            case LFS_TYPE_REG:
                printf("-rw-\t");
                break;
            case LFS_TYPE_DIR:
                printf("drw-\t");
                break;
            default:
                printf("?---\t");
                break;
        }

        static const char *prefixes[] = {"", "K", "M", "G"};
        for (int i = sizeof(prefixes) / sizeof(prefixes[0]) - 1; i >= 0; i--) {
            if (info.size >= (1 << 10 * i) - 1) {
                printf("%*u%s\t", 4 - (i != 0), info.size >> 10 * i, prefixes[i]);
                break;
            }
        }

        printf("%s\n", info.name);
    }

    err = lfs_dir_close(&lfs, &dir);

    lfs_ssize_t totalsize = lfs_fs_size(&lfs);
    printf("Total filesystem size is %u bytes\n", totalsize);

    if (err) {
        return err;
    }

    return 0;
}

/**
 * Create a new directory
 * @param path directory path
 * @return possible error code
 */
int mkdir(const char *path) {
    return lfs_mkdir(&lfs, path);
}

/**
 * Remove a file or an empty directory
 * @param path file path
 * @return possible error code
 */
int rm(const char *path) {
    return lfs_remove(&lfs, path);
}

/**
 *
 * @param path to current file or directory
 * @param path to new file or directory
 * @return possible error code
 */
int mv(char *oldpath, char *newpath) {
    return lfs_rename(&lfs, oldpath, newpath);
}

/**
 * Create an empty file
 * @param path file path
 * @return possible error code
 */
int touch(char *path) {
    fileopen(path, LFS_O_CREAT);
    fileclose();
}


/**
 * Check if command string was valid
 * @param input user input
 * @param cmd command to look for
 * @return true if command was valid
 */
int checkcmd(char *input, char *cmd) {
    if (strncmp(input, cmd, strlen(cmd)) == 0) return 1;
    return 0;
}

/**
 * Return first parameter (usually filename) from user input
 * @param input
 * @return
 */
char *getparameters(char *input) {
    input = strtok(input, " ");
    input = strtok(NULL, " ");
    if (input == NULL) input = " ";
    return input;
}

/**
 * Remove part of a string
 * @param str string to modify
 * @param begin begin position
 * @param len length to remove
 * @return length of string
 */
int strcut(char *str, int begin, int len) {
    int l = strlen(str);

    if (len < 0) len = l - begin;
    if (begin + len > l) len = l - begin;
    memmove(str + begin, str + begin + len, l - len + 1);

    return len;
}

/**
 * Return string representation from an error code
 * @param err error code
 */
void checkerror(enum lfs_error err) {
    if (err < 0) {
        printf("Error: ");
        switch (err) {
            case LFS_ERR_OK:
                break;
            case LFS_ERR_IO:
                printf("Error during device operation");
                break;
            case LFS_ERR_CORRUPT:
                printf("Data corrupted");
                break;
            case LFS_ERR_NOENT:
                printf("No directory or file entries");
                break;
            case LFS_ERR_EXIST:
                printf("Directory or file already exists");
                break;
            case LFS_ERR_NOTDIR:
                printf("Entry is not an directory");
                break;
            case LFS_ERR_ISDIR:
                printf("Entry is a directory");
                break;
            case LFS_ERR_NOTEMPTY:
                printf("Directory is not empty");
                break;
            case LFS_ERR_BADF:
                printf("Bad file number");
                break;
            case LFS_ERR_FBIG:
                printf("File too large");
                break;
            case LFS_ERR_INVAL:
                printf("Invalid parameter");
                break;
            case LFS_ERR_NOSPC:
                printf("No space left on device");
                break;
            case LFS_ERR_NOMEM:
                printf("No more memory available");
                break;
            case LFS_ERR_NOATTR:
                printf("No data/attribute available");
                break;
            case LFS_ERR_NAMETOOLONG:
                printf("File name too long");
                break;
            default:
                break;
        }
        printf("\n");
    }
}

int main(void) {
    fp = fopen(FILENAME, "r+");
    if (mount() < 0) {
        printf("ERR: Critical failure while mounting filesystem\n");
        return 1;
    }


    char *input = NULL; // input buffer
    size_t num_chars = 0; // input buffer length when read from user
    char *parameters = NULL; // first parameter after a command. probably usually a filename!
    char *inputcopy = NULL; // copy of the input buffer
    char *buffer = NULL; // output buffer
    size_t capline = 0; // not used...

    int ret = 0; // return value from filesystem commands

    while (1) {

        // very simple command interpreter

        printf("%% ");

        // get input from user
        num_chars = getline(&input, &capline, stdin);
        ret = 0;

        // make a copy of the input buffer
        inputcopy = malloc(strlen(input));
        memcpy(inputcopy, input, strlen(input));

        // get rid of LF, CR, CRLF, etc...
        input[strcspn(input, "\r\n")] = 0;
        inputcopy[strcspn(inputcopy, "\r\n")] = 0;

        // get first parameter, usually the filename
        parameters = getparameters(input);

        // check for possible command
        if (num_chars > 1) {
            if (checkcmd(input, "exit")) {
                // Exit the program
                break;
            } else if (checkcmd(input, "ls") || checkcmd(input, "dir")) {
                // List directories and files
                if (strcmp(parameters, " ") == 0) parameters = ".";
                ret = lsdir(parameters);
            } else if (checkcmd(input, "mkdir")) {
                // Create a new directory
                if (strlen(parameters) > 0) ret = mkdir(parameters);
            } else if (checkcmd(input, "rm")) {
                // Remove a directory or a file
                if (strlen(parameters) > 0) ret = rm(parameters);
            } else if(checkcmd(input, "mv")) {
                strcut(inputcopy, 0, 4+strlen(parameters));
                ret = mv(parameters, inputcopy);
            } else if (checkcmd(input, "touch")) {
                // Create an empty file
                if (strlen(parameters) > 0) ret = touch(parameters);
            } else if (checkcmd(input, "write")) {
                // Write to a file. Old content is overwritten
                ret = fileopen(parameters, LFS_O_WRONLY | LFS_O_CREAT);

                // Remove command and filename from the (copy)input buffer
                strcut(inputcopy, 0, 7 + strlen(parameters));
                if (ret == 0) ret = filewrite(inputcopy, strlen(inputcopy));
                if (ret >= 0) printf("%u bytes writen\n", ret);
                fileclose();
            } else if (checkcmd(input, "append")) {
                // Append data to a file
                ret = fileopen(parameters, LFS_O_RDWR | LFS_O_APPEND | LFS_O_CREAT);

                // Remove command and filename from the (copy)input buffer
                strcut(inputcopy, 0, 8 + strlen(parameters));
                if (ret == 0) ret = filewrite(inputcopy, strlen(inputcopy));
                if (ret >= 0) printf("%u bytes writen\n", ret);
                fileclose();
            } else if (checkcmd(input, "cat")) {
                // Print contents of a file
                ret = fileopen(parameters, LFS_O_RDONLY);

                if (ret == 0) {
                    buffer = malloc(file.ctz.size);
                    ret = fileread(buffer, file.ctz.size);
                    if (ret > 0) printf("%s\n%u bytes read\n", buffer, ret);
                    else
                        printf("0 bytes read\n");
                    free(buffer);
                }
                fileclose();
            } else if (checkcmd(input, "clear")) {
                // attempt to clear screen (linux only)
                printf("\e[1;1H\e[2J");
            } else {
                if (!checkcmd(input, "help")) printf("Unknown command. ");
                printf("Valid commands are:\n\n");
                printf("help\t\t\t\tthis screen\n");
                printf("exit\t\t\t\texit the \"shell\"\n");
                printf("clear\t\t\t\tclears the screen\n");
                printf("ls [path]\t\t\tlist the contents of a directory\n");
                printf("dir [path]\t\t\tsee ls\n");
                printf("mkdir [path}\t\t\tcreate directory\n");
                printf("rm [path]\t\t\tremove file or directory\n");
                printf("mv [src] [dest]\t\tmove file or directory (works as a rename too)");
                printf("touch [filename]\t\tcreate empty file\n");
                printf("append [filename] [text]\tappend text into a file\n");
                printf("write [filename] [text]\t\twrite text into a file overwriting previous\n");
                printf("cat [filename]\t\t\tprint contents of a file\n");
                printf("\n");
            }
            // Check for possible errors (negative ret)
            checkerror(ret);
            free(inputcopy);
        }
    }

    umount();
    fclose(fp);
    return 0;
}